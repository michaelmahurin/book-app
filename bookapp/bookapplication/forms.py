from django import forms
from .models import Book, Magazine


class Bookform(forms.ModelForm):

    class Meta:
        model = Book
        fields = ['title','authors', 'isbn', 'pages', 'year_published', 'image']



class Magazineform(forms.ModelForm):

    class Meta:
        model = Magazine
        fields = ['title','description', 'releasecycle', 'image', 'genre']

