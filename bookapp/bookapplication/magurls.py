from django.contrib import admin
from django.urls import path, include
from bookapplication.views import show_magazines, create_magazines, magazine_details, delete_magazine, update_magazine
 
 
 
 
 
urlpatterns = [

    path('', show_magazines, name = "show_magazines" ),
    path('create_mag/', create_magazines, name = 'create_mag'),
    path('<int:pk>/', magazine_details, name = "magazine_details"),
    path('<int:pk>/delete/', delete_magazine, name = 'delete_magazine'),
    path('<int:pk>/update/' , update_magazine, name = "update_magazine"),
    




 ]