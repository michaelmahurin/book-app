from django.contrib import admin
from django.urls import path, include
from bookapplication.views import book_details, show_books, delete_book, update_book


urlpatterns = [
    path('', show_books, name ="show_books"),
    path('<int:pk>/', book_details, name = 'book_details'),
    path('<int:pk>/delete/', delete_book, name = 'delete_book'),
    path('<int:pk>/update/' , update_book, name = "update_book")



    
    
]