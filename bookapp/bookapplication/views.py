from multiprocessing import context
from pickle import NONE
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from .models import Book, Magazine
from .forms import Bookform, Magazineform



def show_books(request):
    books = Book.objects.all
    context = {
        'books': books
    }
    return render(request, "books/list.html", context)


def create_view(request):
    context = {}
    form = Bookform(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("show_books")
    context['form'] = form
    return render(request, "books/create.html", context)


def book_details(request, pk):
    context = {'book': Book.objects.get(pk = pk) if Book else None,}
    return render(request, "books/detail.html", context)


def delete_book(request, pk):
    context = {'book' : Book.objects.all()}
    obj = get_object_or_404(Book, pk = pk)
    if request.method == 'POST':
        obj.delete()
        return HttpResponseRedirect("show_books")
    return render(request,"books/delete.html", context)


def update_book(request, pk):
    book = get_object_or_404(Book, pk=pk)
    form = Bookform(request.POST or None, instance=book)
    if form.is_valid():
        form.save()
        return redirect("show_books")
    context = {
        "form": form
    }
    return render(request, 'books/update.html', context)




    
        


    
    




def show_magazines(request):
    magazines = Magazine.objects.all()
    context = {
        "magazines" : magazines
    }
    return render(request, 'magazines/list.html', context)



def create_magazines(request):
    context = {}
    form = Magazineform(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("show_magazines")
    context['form'] = form
    return render(request, "magazines/create.html", context)




def magazine_details(request, pk):
    context = {'magazine': Magazine.objects.get(pk = pk) if Magazine else None,}
    return render(request, "magazines/details.html", context)



def delete_magazine(request, pk):
    context = {'magazine' : Magazine.objects.all()}
    obj = get_object_or_404(Magazine, pk = pk)
    if request.method == 'POST':
        obj.delete()
        return redirect("show_magazines")
    return render(request,"magazines/delete.html", context)


def update_magazine(request, pk):
    magazine = get_object_or_404(Magazine, pk=pk)
    form = Magazineform(request.POST or None, instance=magazine)
    if form.is_valid():
        form.save()
        return redirect("show_magazines")
    context = {
        "form": form
    }
    return render(request, 'magazines/update.html', context)







